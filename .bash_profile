# Shell colors
. hopscotch.sh
alias ls="ls -G"

# RSpec colors
alias rspec="rspec --color"

# rbenv
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"

# Git autocomplete
source ~/.git-completion.bash