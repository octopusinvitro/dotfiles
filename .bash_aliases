# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto --group-directories-first'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# tree
alias tree='tree -aC --dirsfirst -I "node_modules|.git"'

# bundle
alias be="bundle exec"
alias rspec="clear && rspec --color"
alias beproud="clear && be rake test TESTOPTS='--pride'"
alias runtests="clear && be rspec --color"

# In /etc/environment, add this: export DATOS="PATH_HERE"
alias datos="   cd "$DATOS
alias dropbox=" cd "$DATOS"/Dropbox/"
alias dotfiles="cd "$DATOS"/Dropbox/.dotfiles"
alias megasync="cd "$DATOS"/megasync"
alias repos="   cd "$DATOS"/repos/"

# git
alias g='git log --graph --full-history --all --color --pretty=tformat:"%x1b[31m%h%x09%x1b[32m%d%x1b[0m%x20%s%x20%x1b[33m(%an)%x1b[0m"'
alias setuser="git config user.name 'octopusinvitro' && git config user.email 'octopusinvitro@users.noreply.github.com'"
delete() {
  git push origin :$1
  git branch -D $1
}

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'


# -=-=-=-=- EVERY POLITICIAN -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# Bash alias does not directly accept parameters. You have to create a function
# and alias that. alias does not accept parameters but it can alias a function
# that does. For example:

countries() {
  pushd ../../../; EP_COUNTRY_REFRESH=$1 be rake countries.json; git add countries.json; git commit -m "Refresh countries.json"; popd;
}
# alias country=countries

# By the way, Bash functions defined in your .bashrc and other files are
# available as commands within your shell. So for instance you can call the
# earlier function like this

# $ myfunction original.conf my.conf

bot() {
  heroku run "ruby -r ./app -e 'PullRequestReview.perform_async($*)'" -a everypolitician-pr-summarizer
}

sidekiq-pass() {
  heroku config:get SIDEKIQ_PASSWORD --app $1
}

sidekiq() {
  heroku apps:open sidekiq --app $1
}

sql2csv() {
  sqlite3 data.sqlite <<!
.headers on
.mode csv
.output $1
SELECT * FROM data ORDER BY id;
!
}


alias beclean="be rake clean default"
alias bescraper="be ruby scraper.rb"
